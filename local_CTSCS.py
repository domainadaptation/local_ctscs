
# coding: utf-8

# In[1]:

from numpy import genfromtxt
import pandas as pd
from statistics import amplitude
from statistics import beyond1std
from statistics import flux_percentile_ratio_mid20
from statistics import flux_percentile_ratio_mid35
from statistics import flux_percentile_ratio_mid50
from statistics import flux_percentile_ratio_mid65
from statistics import flux_percentile_ratio_mid80
from statistics import linear_trend
from statistics import max_slope
from statistics import median_absolute_deviation
from statistics import median_buffer_range_percentage
from statistics import pair_slope_trend
from statistics import percent_amplitude
from statistics import percent_difference_flux_percentile
from statistics import qso
from statistics import skew
from statistics import small_kurtosis
from statistics import std
from statistics import stetson_j
from statistics import stetson_k
from statistics import ls
import xml.etree.ElementTree as ET
from xml.dom import minidom

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


# In[2]:

input_filename = 'lineardb_lc_data_over40.csv'
my_data = pd.read_csv(input_filename, names=['id', 'MJD', 'mag', 'magerr'])
output_filename = 'lc_'+input_filename

#using an array to represent the feature selected
selected_features = range(1,22)
selected_features.remove(15)

# Start with an empty dictionary
stats = {}
dict_list = []
for line in open('config.dat'):
    # Split the config.dat file with delimiter ','; key is the feature number and value is feature name
    line = line.replace("'", "")
    temp = line.rstrip().split(',')
    stats[temp[0]] = temp[1]
    d = {}
    d['ID'] = temp[1]
    d['datatype'] = temp[2]
    d['name'] = temp[3]
    dict_list.append(d)


# In[3]:

#CHOOSE if we want the output file to be .csv or .xml
csv = True
if csv:
    output_csv = open(output_filename + '.csv','w')
    output_csv.write('')


# In[4]:

tree = ET.parse('lc_basic_format.xml')
root = tree.getroot()
table = root.find('RESOURCE').find('TABLE')


# In[5]:

for f in selected_features:
    ET.SubElement(table, "FIELD", attrib=dict_list[f-1])


# In[6]:

Ids = my_data['id'].unique()

e_data = ET.SubElement(table, 'DATA')
e_tbdata = ET.SubElement(e_data, 'TABLEDATA')


# In[7]:

for id in Ids:
    data = ((my_data[my_data['id'] == id])[['MJD','mag','magerr']]).transpose().as_matrix()
    if csv:
        row_list = [str(id)]
    else:
        e_tr = ET.SubElement(e_tbdata, 'TR')
        (ET.SubElement(e_tr, 'TD')).text = str(id)
    for feature in selected_features:
        method = stats[str(feature)]
        value = str(eval(method)(data))
        if csv:
            row_list.append(value)
        else:
            (ET.SubElement(e_tr, 'TD')).text = value
    if csv:
        output_csv.write(','.join(row_list))
        output_csv.write('\n')


# In[ ]:

if csv:
    output_csv.close()
else:
    #tree.write(output_filename+'.xml')
    print prettify(root) > (output_filename+'.xml')


# In[20]:

'''
example = my_data[my_data['id'].isin(Ids)]
example.to_csv('delete.csv',header=False,index=False)
'''

