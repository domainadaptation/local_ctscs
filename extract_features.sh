#!/bin/bash

i=0
for file in ./*.csv
do
	i=$((i+1))
	curl -s -d @LC_3591947_f.csv "http://nirgun.caltech.edu:8000/analyze?format=csv&stats=1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,23,24,26" > ${file%.csv}.xml 
	perc=$(((i*100)/65708))
	echo -ne "$perc% done, $i/65708 done. \r"
done
